<div id="header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left auto-width-left">
                <ul class="top-menu menu-beta l-inline">
                    <li><a href=""><i class="fa fa-home"></i> 90-92 Lê Thị Riêng, Bến Thành, Quận 1</a></li>
                    <li><a href=""><i class="fa fa-phone"></i> 0163 296 7751</a></li>
                </ul>
            </div>
            <div class="pull-right auto-width-right">
                <ul class="top-details menu-beta l-inline">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <li><a href="#"><i class="fa fa-user"></i>{{\Illuminate\Support\Facades\Auth::user()->email}}</a></li>
                        @else
                        <li><a href="{{route('dangki')}}">Đăng kí</a></li>
                        <li><a href="{{route('dangnhap')}}">Đăng nhập</a></li>
                    @endif
                </ul>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-top -->
    <div class="header-body">
        <div class="container beta-relative">
            <div class="pull-left">
                <a href="index.html" id="logo"><img src="source/assets/dest/images/logo-cake.png" width="200px" alt=""></a>
            </div>
            <div class="pull-right beta-components space-left ov">
                <div class="space10">&nbsp;</div>
                <div class="beta-comp">
                    <form role="search" method="get" id="searchform" action="/">
                        <input type="text" value="" name="s" id="s" placeholder="Nhập từ khóa..." />
                        <button class="fa fa-search" type="submit" id="searchsubmit"></button>
                    </form>
                </div>

                <div class="beta-comp">
                    <div class="cart">
                        {{--@if($cart)--}}
                                {{--{{var_dump($cart)}}--}}
                            {{--@endif--}}
                        <div class="beta-select"><i class="fa fa-shopping-cart"></i> Giỏ hàng ({{$cart==null?"Trống":($cart->totalNumber.' sản phẩm')}}) <i class="fa fa-chevron-down"></i></div>
                        @if($cart)
                                <div class="beta-dropdown cart-body">
                                    @foreach($cart->listItem as $item)
                                        {{--{{var_dump($item['item']->attributes[''])}}--}}
                                    <div class="cart-item">
                                        <a href="{{route('xoagiohang',$item['item']['id'])}}" class="cart-item-delete"><i class="fa fa-times"></i></a>
                                        <div class="media">
                                            <a class="pull-left" href="#"><img src="source/image/product/{{$item['item']['image']}}" alt="Not found" width="50px" height="50px"></a>
                                            <div class="media-body">
                                                <span class="cart-item-title">{{$item['item']['name']}}</span>
                                                {{--<span class="cart-item-options">Size: XS; Colar: Navy</span>--}}
                                                <span class="cart-item-amount">{{$item['soluong']}}*<span>${{$item['price']}}</span></span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="cart-caption">
                                        <div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">${{$cart->totalPrice}}</span></div>
                                        <div class="clearfix"></div>

                                        <div class="center">
                                            <div class="space10">&nbsp;</div>
                                            <a href="{{route('checkout')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            @endif

                    </div> <!-- .cart -->
                </div>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> <!-- .header-body -->
    <div class="header-bottom" style="background-color: #0277b8;">
        <div class="container">
            <a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
            <div class="visible-xs clearfix"></div>
            <nav class="main-menu">
                <ul class="l-inline ov">
                    <li><a href="{{route('trangchu')}}">Trang chủ</a></li>
                    <li><a href="#">Loại sản phẩm</a>
                        <ul class="sub-menu">
                            @foreach($loai_sp as $loai)
                                <li><a href="{{route('loaisanpham',$loai->id)}}">{{$loai->name}}</a></li>
                                @endforeach
                            {{--<li><a href="product_type.html">Sản phẩm 1</a></li>--}}
                            {{--<li><a href="product_type.html">Sản phẩm 2</a></li>--}}
                            {{--<li><a href="product_type.html">Sản phẩm 4</a></li>--}}
                        </ul>
                    </li>
                    <li><a href="{{route('gioithieu')}}">Giới thiệu</a></li>
                    <li><a href="{{route('lienhe')}}">Liên hệ</a></li>
                </ul>
                <div class="clearfix"></div>
            </nav>
        </div> <!-- .container -->
    </div> <!-- .header-bottom -->
</div>