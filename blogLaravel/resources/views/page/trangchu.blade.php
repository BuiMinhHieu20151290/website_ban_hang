@extends('master');
@section('content')
<div class="rev-slider">
    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner">
            <div class="bannercontainer" >
                <div class="banner" >
                    <ul>

                        <!-- THE FIRST SLIDE -->
                        @foreach($slide as $item)
                        <li data-transition="boxfade" data-slotamount="20" class="active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0;">
                            <div class="slotholder" style="width:100%;height:100%;" data-duration="undefined" data-zoomstart="undefined" data-zoomend="undefined" data-rotationstart="undefined" data-rotationend="undefined" data-ease="undefined" data-bgpositionend="undefined" data-bgposition="undefined" data-kenburns="undefined" data-easeme="undefined" data-bgfit="undefined" data-bgfitend="undefined" data-owidth="undefined" data-oheight="undefined">
                                <div class="tp-bgimg defaultimg" data-lazyload="undefined" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" data-lazydone="undefined" src="source/image/slide/{{$item->image}}" data-src="source/image/slide/{{$item->image}}" style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url({{"source/image/slide/".$item->image}}); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit;">
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="tp-bannertimer"></div>
        </div>
    </div>
    <!--slider-->
</div>
<div class="container">
    <div id="content" class="space-top-none">
        <div class="main-content">
            <div class="space60">&nbsp;</div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="beta-products-list">
                        <h4>New Products</h4>
                        <div class="beta-products-details">
                            <p class="pull-left">Tìm thấy {{count($new_Product)}} sản phẩm</p>
                            <div class="clearfix"></div>
                        </div>

                            @for($i=0;$i<count($new_Product)/4;$i++)
                                <div class="row">
                                    @for($j=0;$j<4;$j++)
                                        @if((4*$i+$j)<count($new_Product))
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                @if($new_Product[(4*$i+$j)]->promotion_price!=0)
                                                    <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                                    @endif
                                                <div class="single-item-header">
                                                    <a href="{{route('chitietsanpham',$new_Product[(4*$i+$j)]->id)}}"><img src="source/image/product/{{$new_Product[(4*$i+$j)]->image}} " width="270px" height="320px" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">{{$new_Product[(4*$i+$j)]->name}}</p>
                                                    <p class="single-item-price">
                                                        @if($new_Product[(4*$i+$j)]->promotion_price==0)
                                                            <span class="flash-sale">{{$new_Product[(4*$i+$j)]->unit_price}}</span>
                                                            @else
                                                        <span class="flash-del">{{$new_Product[(4*$i+$j)]->promotion_price}}</span>
                                                        <span class="flash-sale">{{$new_Product[(4*$i+$j)]->unit_price}}</span>
                                                            @endif
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="{{route('themgiohang',$new_Product[(4*$i+$j)]->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="{{route('chitietsanpham',$new_Product[(4*$i+$j)]->id)}}">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    @endfor
                                </div>
                                <div class="space40">&nbsp;</div>
                            @endfor
                        <div class="row">{{$new_Product->links()}}</div>
                    </div> <!-- .beta-products-list -->

                    <div class="space50">&nbsp;</div>

                    <div class="beta-products-list">
                        <h4>Top Products</h4>
                        <div class="beta-products-details">
                            <p class="pull-left">Tìm thấy {{count($top_Product)}} sản phẩm</p>
                            <div class="clearfix"></div>
                        </div>
                        @for($i=0;$i<count($top_Product)/4;$i++)
                            <div class="row">
                                @for($j=0;$j<4;$j++)
                                    @if((4*$i+$j)<count($top_Product))
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                @if($top_Product[(4*$i+$j)]->promotion_price!=0)
                                                    <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                                @endif
                                                <div class="single-item-header">
                                                    <a href="{{route('chitietsanpham',$top_Product[(4*$i+$j)]->id)}}"><img src="source/image/product/{{$top_Product[(4*$i+$j)]->image}} " width="270px" height="320px" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">{{$top_Product[(4*$i+$j)]->name}}</p>
                                                    <p class="single-item-price">
                                                        @if($top_Product[(4*$i+$j)]->promotion_price==0)
                                                            <span class="flash-sale">{{$top_Product[(4*$i+$j)]->unit_price}}</span>
                                                        @else
                                                            <span class="flash-del">{{$top_Product[(4*$i+$j)]->promotion_price}}</span>
                                                            <span class="flash-sale">{{$top_Product[(4*$i+$j)]->unit_price}}</span>
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="{{route('themgiohang',$top_Product[(4*$i+$j)]->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="{{route('chitietsanpham',$top_Product[(4*$i+$j)]->id)}}">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endfor
                            </div>
                            <div class="space40">&nbsp;</div>
                        @endfor
                        <div class="row">{{$top_Product->links()}}</div>
                    </div> <!-- .beta-products-list -->
                </div>
            </div> <!-- end section with sidebar and main content -->


        </div> <!-- .main-content -->
    </div>
</div> <!-- .container -->
     <!-- #content -->
@endsection