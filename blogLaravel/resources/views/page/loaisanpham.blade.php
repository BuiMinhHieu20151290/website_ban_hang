@extends('master');
@section('content')
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Sản phẩm</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="index.html">Home</a> / <span>Sản phẩm</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                <div class="space60">&nbsp;</div>
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="aside-menu">
                            @foreach($ds_loaisp as $loai)
                                <li><a href="{{route('loaisanpham',$loai->id)}}">{{$loai->name}}</a></li>
                                @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-9">
                        <div class="beta-products-list">
                            <h4>Sản phẩm {{$name[0]->name}}</h4>
                            <div class="beta-products-details">
                                <p class="pull-left">Tìm thấy {{count($ds_sanpham)}} sản phẩm</p>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                @foreach($ds_sanpham as $key=>$sanpham)
                                    <div class="col-sm-4" style="margin-bottom: 20px">
                                        <div class="single-item" >
                                            @if($sanpham->promotion_price!=0)
                                                <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                            @endif
                                            <div class="single-item-header">
                                                <a href="{{route('chitietsanpham',$sanpham->id)}}"><img src="source/image/product/{{$sanpham->image}}" width="270px" height="320px" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title">{{$sanpham->name}}</p>
                                                <p class="single-item-price">
                                                    @if($sanpham->promotion_price==0)
                                                        <span class="flash-sale">{{$sanpham->unit_price}}</span>
                                                        @else
                                                        <span class="flash-del">{{$sanpham->promotion_price}}</span>
                                                        <span class="flash-sale">{{$sanpham->unit_price}}</span>
                                                        @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="{{route('themgiohang',$sanpham->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="{{route('chitietsanpham',$sanpham->id)}}">Details <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--@if($key%2==0)--}}
                                        {{--<div class="space40">&nbsp;</div>--}}
                                        {{--@endif--}}
                                    @endforeach

                            </div>
                        </div> <!-- .beta-products-list -->

                        <div class="space50">&nbsp;</div>

                        <div class="beta-products-list">
                            <h4>Sản phẩm khác</h4>
                            <div class="beta-products-details">
                                <p class="pull-left">Tìm thấy {{count($ds_sanphamkhac)}} sản phẩm</p>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                @foreach($ds_sanphamkhac as $key=>$sanpham)
                                    <div class="col-sm-4" style="margin-bottom: 20px">
                                        <div class="single-item" >
                                            @if($sanpham->promotion_price!=0)
                                                <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>
                                            @endif
                                            <div class="single-item-header">
                                                <a href="{{route('chitietsanpham',$sanpham->id)}}"><img src="source/image/product/{{$sanpham->image}}" width="270px" height="320px" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title">{{$sanpham->name}}</p>
                                                <p class="single-item-price">
                                                    @if($sanpham->promotion_price==0)
                                                        <span class="flash-sale">{{$sanpham->unit_price}}</span>
                                                    @else
                                                        <span class="flash-del">{{$sanpham->promotion_price}}</span>
                                                        <span class="flash-sale">{{$sanpham->unit_price}}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="{{route('themgiohang',$sanpham->id)}}"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="{{route('chitietsanpham',$sanpham->id)}}">Details <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--@if($key%2==0)--}}
                                    {{--<div class="space40">&nbsp;</div>--}}
                                    {{--@endif--}}
                                @endforeach
                            </div>
                            <div class="space40">&nbsp;</div>
                            <div>{{$ds_sanphamkhac->links()}}</div>
                        </div> <!-- .beta-products-list -->
                    </div>
                </div> <!-- end section with sidebar and main content -->


            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div> <!-- .container -->
    @endsection