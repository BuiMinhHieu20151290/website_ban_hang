<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\c_loaitin;

Route::get('index',[
    'as'=>'trangchu',
    'uses'=>'PageController@getIndex'
]);
Route::get('loaisanpham/{type}',[
    'as'=>'loaisanpham',
    'uses'=>'PageController@getLoaiSanPham'
]);
Route::get('chitietsanpham/{id}',[
    'as'=>'chitietsanpham',
    'uses'=>'PageController@getChiTietSanPham'
]);
Route::get('lienhe',[
    'as'=>'lienhe',
    'uses'=>'PageController@getLienHe'
]);
Route::get('gioithieu',[
    'as'=>'gioithieu',
    'uses'=>'PageController@getGioiThieu'
]);
Route::get('themgiohang/{id}',[
    'as'=>'themgiohang',
    'uses'=>'PageController@getThemGioHang'
]);
Route::get('xoa',function(){
    Session::forget('cart');
});
Route::get('xoagiohang/{id}',[
    'as'=>'xoagiohang',
    'uses'=>'PageController@getXoaGioHang'
]);
Route::get('checkout',[
    'as'=>'checkout',
    'uses'=>'PageController@getCheckOut'
]);
Route::post('checkout',[
    'as'=>'checkout',
    'uses'=>'PageController@postCheckOut'
]);
Route::get('dangnhap',[
    'as'=>'dangnhap',
    'uses'=>'PageController@dangnhap'
]);
Route::post('dangnhap',[
    'as'=>'dangnhap',
    'uses'=>'PageController@postdangnhap'
]);
Route::get('dangki',[
    'as'=>'dangki',
    'uses'=>'PageController@dangki'
]);
Route::post('dangki',[
    'as'=>'dangki',
    'uses'=>'PageController@postdangki'
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
