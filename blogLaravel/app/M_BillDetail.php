<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_BillDetail extends Model
{
    //
    protected $table='bill_detail';
    public function belongToBill(){
        return $this->belongsTo('App\M_Bills',"id_bill","id");
    }
    public function belongToProduct(){
        return $this->belongsTo('App\M_Products',"id_product","id");
    }
}
