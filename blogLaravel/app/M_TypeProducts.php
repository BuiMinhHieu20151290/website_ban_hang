<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_TypeProducts extends Model
{
    //
    protected $table="type_products";
    public function hasManyProduct(){
        return $this->hasMany("App\M_Products","id_type","id");
    }
}
