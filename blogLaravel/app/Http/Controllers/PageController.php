<?php

namespace App\Http\Controllers;

use App\M_BillDetail;
use App\M_Bills;
use App\M_Customer;
use App\M_Users;
use Illuminate\Http\Request;
use App\M_Slide;
use App\M_Products;
use App\M_TypeProducts;
use App\InfoCart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class PageController extends Controller
{
    //
    public function getIndex(){
        $slide=M_Slide::all();
        $new_Product=M_Products::where('new',1)->paginate(4);
        $top_Product=M_Products::where('promotion_price','<>',0)->paginate(8);
        return view('page.trangchu',['slide'=>$slide,'new_Product'=>$new_Product,'top_Product'=>$top_Product]);
    }
    public function getLoaiSanPham($type){
        $ds_loaisanpham=M_TypeProducts::all();
        $ds_sanpham=M_Products::where('id_type',$type)->get();
        $name=M_TypeProducts::where('id',$type)->get();
        $ds_sanphamkhac=M_Products::where('id_type','<>',$type)->paginate(3);
//        print_r($name);
        //print_r($ds_sanpham);
        return view('page.loaisanpham',['ds_sanpham'=>$ds_sanpham,'name'=>$name,'ds_sanphamkhac'=>$ds_sanphamkhac,'ds_loaisp'=>$ds_loaisanpham]);
}
    public function getChiTietSanPham(Request $request){
        $product= M_Products::where('id',$request->id)->first();
        $ds_product=M_Products::where('id_type',$product->id_type)->paginate(3);
        return view('page/chitietsanpham',['product'=>$product,'ds_product'=>$ds_product]);
    }
    public function getLienHe(){
        return view('page.lienhe');
    }
    public function getGioiThieu(){
        return view('page.gioithieu');
    }
    public function getThemGioHang($id){
        $product=M_Products::find($id);
        $oldCart= Session('cart')?Session('cart'):null;
        $newCart= new InfoCart($oldCart);
        $newCart->addProduct($id,$product);
        Session(['cart'=>$newCart]);
        return redirect()->back();
    }
    public function getXoaGioHang(Request $req,$id){
        if($req->session()->has('cart')){
            $oldCart=Session('cart');
            $newCart=new InfoCart($oldCart);
            $newCart->reduceOne($id);
            if($newCart->totalPrice<=0){
                $req->session()->forget('cart');
            }else{
                $req->session()->put('cart',$newCart);
            }
        }
        return redirect()->back();
    }
    public function getCheckOut(Request $req){
        $cart=null;
        if($req->session()->has('cart')){
            $cart=$req->session()->get('cart');
            //dd($cart);
        }
        return view('page.dathang',['cart'=>$cart]);
    }
    public function postCheckOut(Request $req){
        $cart=$req->session()->get('cart');

        $name=$req->name;
        $gender=$req->gender;
        $email=$req->email;
        $address=$req->address;
        $numberphone=$req->phone;
        $note=$req->note;
        $customer= new M_Customer();
        $customer->name=$name;
        $customer->gender=$gender;
        $customer->email=$email;
        $customer->address=$address;
        $customer->phone_number=$numberphone;
        $customer->note=$note;
        $customer->save();

        $id_customer=$customer->id;
        $date= date('Y-m-d');
        $total=$cart->totalPrice;
        $payment=$req->payment_method;
        $bill= new M_Bills();
        $bill->id_customer=$id_customer;
        $bill->date_order=$date;
        $bill->total=$total;
        $bill->payment=$payment;
        $bill->note=$note;
        $bill->save();

        $listItem=$cart->listItem;
        $id_bill=$bill->id;
        foreach ($listItem as $item){
            $quantity=$item['soluong'];
            $id_product=$item['item']['id'];
            $unit_price=$item['item']['unit_price'];
            $billDetail= new M_BillDetail();
            $billDetail->id_bill= $id_bill;
            $billDetail->id_product=$id_product;
            $billDetail->quantity=$quantity;
            $billDetail->unit_price=$unit_price;
            $billDetail->save();
        }
        $req->session()->forget('cart');
        return redirect()->back()->with('thongbao','Đặt hàng thành công');
    }
    public function dangnhap(){
        return view('page.dangnhap');
    }
    public function postdangnhap(Request $req){
        $this->validate($req,[
            'email'=>'required|email',
            'password'=>'required|min:6|max:20'
        ]);
        $credential= array('email'=>$req->email,'password'=>$req->password);
//        dd(['emai'=>$req->email,'passowrd'=>md5($req->password)]);
        if(Auth::attempt($credential)){
            return redirect('index');
        }else{
            return redirect()->back()->with(['thongbao'=>'Danh nhap khong thanh cong']);
        }
    }
    public function dangki(){
        return view('page.dangki');
    }
    public function postdangki(Request $req){
        $this->validate($req,[
            'email'=>'required|email|unique:users',
            'name'=>'required',
            'password'=>'required|min:6|max:20',
            'address'=>'required',
            'phone'=>'required',
            'repassword'=>'required|same:password'
            ]);
        $user= new M_Users();
        $user->email=$req->email;
        $user->full_name=$req->name;
        $user->address=$req->address;
        $user->password= Hash::make($req->password);
        $user->phone=$req->phone;
        $user->save();
        return redirect()->back()->with('thongbao','Tạo tài khoản thành công');
    }
}
