<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Products extends Model
{
    //
    protected $table='products';
    public function hasManyBillDetail(){
        return $this->hasMany("App\M_BillDetail","idBill","id");
    }
    public function belongTypeProduct(){
        return $this->belongsTo("App\M_TypeProducts","id_type","id");
    }
}
