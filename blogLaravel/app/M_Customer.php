<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Customer extends Model
{
    //
    protected $table="customer";
    public function hasBills(){
        return $this->hasMany("App\M_Bills","id_customer","id");
    }
}
