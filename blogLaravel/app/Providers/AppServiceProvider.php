<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\M_TypeProducts;
use App\InfoCart;
use mysql_xdevapi\Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('header',function ($view){
            $loai_product=M_TypeProducts::all();
            if(Session('cart')){
                $oldCart=Session('cart');
                $cart=new InfoCart($oldCart);
            }
            $view->with(['loai_sp'=>$loai_product,'cart'=>Session('cart')?Session('cart'):null]);

        });
    }
}
