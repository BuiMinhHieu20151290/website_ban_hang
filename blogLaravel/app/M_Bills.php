<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Bills extends Model
{
    //
    protected $table="bills";
    public function hasManyBillDatail(){
        return $this->hasMany("App\M_BillDetail","id_bill","id");
    }
    public function belongToCustomer(){
        return $this->belongsTo("App\M_Customer","id_customer","id");
    }
}
