<?php
/**
 * Created by PhpStorm.
 * User: hieub
 * Date: 3/1/2019
 * Time: 11:03 PM
 */

namespace App;


use mysql_xdevapi\Session;

class InfoCart
{
    public $listItem=null;
    public $totalNumber=0;
    public $totalPrice=0;
    public function __construct($oldCart)
    {
        if($oldCart){
            $this->listItem=$oldCart->listItem;
            $this->totalNumber=$oldCart->totalNumber;
            $this->totalPrice=$oldCart->totalPrice;
        }
    }
    public function addProduct($id,$item){
        $sanpham=['soluong'=>0,"price"=>$item->unit_price,"item"=>$item];
        if($this->listItem){
            if(array_key_exists($id,$this->listItem)){
                $sanpham=$this->listItem[$id];
            }
        }
        $sanpham['soluong']+=1;
        $sanpham['price']=$sanpham['soluong']*$item->unit_price;
        $this->listItem[$id]=$sanpham;
        $this->totalPrice+=$item->unit_price;
        $this->totalNumber++;
    }
    public function reduceOne($id){
        $sanpham=$this->listItem[$id];
        $sanpham['soluong']-=1;
        $sanpham['price']-=$this->listItem[$id]['item']['unit_price'];
        $this->listItem[$id]=$sanpham;
        $this->totalNumber-=1;
        $this->totalPrice-=$this->listItem[$id]['item']['unit_price'];
        if($this->listItem[$id]['soluong']==0){
            unset($this->listItem[$id]);
        }

//        if($this->totalNumber<=0){
//            Session::forget('cart');
//        }
    }
}